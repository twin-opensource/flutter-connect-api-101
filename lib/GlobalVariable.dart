import 'package:flutter/material.dart';

final String appVersion = "1.0";
final String companyProfile = "Twin Synergy";

final Image imgTwinLogoBlack =
    Image.asset("assets/images/logo-twin-black.png", fit: BoxFit.contain);
final Image imgTwinLogoWhite =
    Image.asset("assets/images/logo-twin-white.png", fit: BoxFit.contain);
final Image imgHomeBG =
    Image.asset("assets/images/homeBG.jpg", fit: BoxFit.cover);
final Image imgRegBG =
    Image.asset("assets/images/registerBG.jpg", fit: BoxFit.cover);

final Text txtAppVersion = Text(
    "Version: $appVersion / Developed by: $companyProfile ",
    style: TextStyle(color: Colors.black, fontSize: 10.0));
